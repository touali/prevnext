<?php
/**
 * @file
 * Contains \Drupal\prevnext\Plugin\Block\PrevNextBlock.
 */
namespace Drupal\prevnext\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Provides a 'PrevNext' block.
 *
 * @Block(
 *   id = "prevnext_block",
 *   admin_label = @Translation("PrevNext Block"),
 *   category = @Translation("Blocks")
 * )
 */
class PrevNextBlock extends BlockBase
{
  const ORDER_ASC = 'ASC';
  const ORDER_DESC = 'DESC';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaultConfig = \Drupal::config('prevnext.settings');

    return [
      'prevnext_default_order' => $defaultConfig->get('prevnext.order'),
      'prevnext_default_previous_label' => $defaultConfig->get('prevnext.links.previous.label'),
      'prevnext_default_previous_classes' => $defaultConfig->get('prevnext.links.previous.classes'),
      'prevnext_default_next_label' => $defaultConfig->get('prevnext.links.next.label'),
      'prevnext_default_next_classes' => $defaultConfig->get('prevnext.links.next.classes'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['prevnext_order'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort order'),
      '#options' => [
        static::ORDER_ASC => $this->t('Ascending'),
        static::ORDER_DESC => $this->t('Descending'),
      ],
      '#default_value' => isset($config['prevnext_order']) ? $config['prevnext_order'] : $config['prevnext_default_order'],
    ];

    $form['prevnext_previous_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Previous link label'),
      '#description' => $this->t('Use this field to override default label text'),
      '#default_value' => isset($config['prevnext_previous_label']) ? $config['prevnext_previous_label'] : $config['prevnext_default_previous_label'],
    ];

    $form['prevnext_previous_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Previous link classes'),
      '#description' => $this->t('Use this field to override default classes'),
      '#default_value' => isset($config['prevnext_previous_classes']) ? $config['prevnext_previous_classes'] : $config['prevnext_default_previous_classes'],
    ];

    $form['prevnext_next_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Next link label'),
      '#description' => $this->t('Use this field to override default label text'),
      '#default_value' => isset($config['prevnext_next_label']) ? $config['prevnext_next_label'] : $config['prevnext_default_next_label'],
    ];

    $form['prevnext_next_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Next link classes'),
      '#description' => $this->t('Use this field to override default classes'),
      '#default_value' => isset($config['prevnext_next_classes']) ? $config['prevnext_next_classes'] : $config['prevnext_default_next_classes'],
    ];

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $this->configuration['prevnext_order'] = $form_state->getValue('prevnext_order');
    $this->configuration['prevnext_previous_label'] = $form_state->getValue('prevnext_previous_label');
    $this->configuration['prevnext_previous_classes'] = $form_state->getValue('prevnext_previous_classes');
    $this->configuration['prevnext_next_label'] = $form_state->getValue('prevnext_next_label');
    $this->configuration['prevnext_next_classes'] = $form_state->getValue('prevnext_next_classes');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $node = \Drupal::request()->attributes->get('node');

    $nids = $this->getPreviousNext($node, $config['prevnext_order']);

    return [
      '#theme' => 'prevnext',
      '#links' => [
        'previous' => [
          'nid' => isset($nids['previous']) ? $nids['previous'] : null,
          'label' => $config['prevnext_previous_label'],
          'classes' => $config['prevnext_previous_classes'],
        ],
        'next' => [
          'nid' => isset($nids['next']) ? $nids['next'] : null,
          'label' => $config['prevnext_next_label'],
          'classes' => $config['prevnext_next_classes'],
        ]
      ],
      '#cache' => [
        'contexts' => ['url'],
      ],
    ];
  }

  /**
   * Based on user chosen config, we define which node is the previous and which one is the next.
   *
   * @param Node $node
   * @param $order
   * @return array
   */
  private function getPreviousNext(Node $node, $order) {
    $nids = [];
    $younger = $this->getYounger($node);
    $older = $this->getOlder($node);

    if ($order === static::ORDER_ASC) {
      if ($younger) {
        $nids['previous'] = $younger;
      }

      if ($older) {
        $nids['next'] = $older;
      }
    }
    elseif ($order === static::ORDER_DESC) {
      if ($older) {
        $nids['previous'] = $older;
      }

      if ($younger) {
        $nids['next'] = $younger;
      }
    }

    return $nids;
  }

  /**
   * We get the younger node after the provided one based on creation time.
   * In case of same created time, we assume that the younger one will be the one which has a lower nid
   *
   * @param Node $node
   * @return mixed|null
   */
  private function getYounger(Node $node) {
    $query = \Drupal::entityQuery('node');
    $language = $node->language()->getId();

    $andCondition = $query
      ->andConditionGroup()
      ->condition('created', $node->getCreatedTime(), '=', $language)
      ->condition('nid', $node->id(), '>', $language);

    $orCondition = $query
      ->orConditionGroup()
      ->condition('created', $node->getCreatedTime(), '>', $language)
      ->condition($andCondition);

    $result = $query
      ->condition('type', $node->getType(), '=', $language)
      ->condition('nid', $node->id(), '!=', $language)
      ->condition($orCondition)
      ->sort('created', 'ASC', $language)
      ->sort('nid', 'ASC', $language)
      ->range(0, 1)
      ->execute();

    if (count($result) > 0) {
      return reset($result);
    }

    return null;
  }

  /**
   * We get the older node after the provided one based on creation time.
   * In case of same created time, we assume that the older one will be the one which has a greater nid
   *
   * @param Node $node
   * @return mixed|null
   */
  private function getOlder(Node $node) {
    $query = \Drupal::entityQuery('node');
    $language = $node->language()->getId();

    $andCondition = $query
      ->andConditionGroup()
      ->condition('created', $node->getCreatedTime(), '=', $language)
      ->condition('nid', $node->id(), '<', $language);

    $orCondition = $query
      ->orConditionGroup()
      ->condition('created', $node->getCreatedTime(), '<', $language)
      ->condition($andCondition);

    $result = $query
      ->condition('type', $node->getType(), '=', $language)
      ->condition('nid', $node->id(), '!=', $language)
      ->condition($orCondition)
      ->sort('created', 'DESC', $language)
      ->sort('nid', 'DESC', $language)
      ->range(0, 1)
      ->execute();

    if (count($result) > 0) {
      return reset($result);
    }

    return null;
  }
}
