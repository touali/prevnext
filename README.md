# PrevNext Block

PrevNext block is a Drupal 8 module that create a custom _prevnext block_.
The purpose of this module is to add a previous and a next link on nodes based on content type and node creation time.

## Usage

Install and enable this module the way you like and place the block where you want. 
Remember that this block is usable on nodes.

### Composer install

Past this in your composer.json repositories 
    
    {
        "type": "vcs",
        "url": "https://bitbucket.org/touali/prevnext"
    }


And then
    
    composer require touali/prevnext


## TODO

* Add error handling when this block is displayed on a non-node content,
* Add a checkbox in block config to handle promoted to list top nodes,
* Add the possibility to display node bundle in complement of previous and next (ex: "Next Article"),
* Add the possibility to display node title instead of previous and next
